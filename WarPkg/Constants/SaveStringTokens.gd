extends Node

const joinGameObjectNameModuleToken = "."  # Used to identify a gameObject's module and name
const endOfStatementToken = ";"
const assignmentToken = "="

const stringEnscapsulationToken = "\""

const ScopeStartToken = "{"
const ScopeEndToken = "}"

const ScopeStartEOLToken = ScopeStartToken + "\n"
const ScopeEndEOLToken = "\n" + ScopeEndToken

const ListStartToken = "["
const ListEndToken = "]"

const UIDToken = "UID"
NAMEToken = "NAME"
MODULEToken = "MODULE"
LOCATIONXToken = "LOCATIONX"
LOCATIONYToken = "LOCATIONY"
RotationToken = "Rotation"

inventoryToken = "inventory"
rpgStatToken = "RPGStats"

UniqueObjectDataTokens = [
	UIDToken,
	NAMEToken,
	MODULEToken,
	LOCATIONXToken,
	LOCATIONYToken,
	RotationToken
]

### Field Types ###
STRING_FIELD = "string"
INT_FIELD = "int"
FLOAT_FIELD = "float"
LIST_FIELD = "list"

##### ObjectDataEntryDataTypes #####
# Note that this might see a lot of overlap with field types and we should probably integrate them at some point
stringObjectDataEntryType = "string"
intObjectDataEntryType = "int"
floatObjectDataEntryType = "float"
listObjectDataEntryType = "list"
gameObjectObjectDataEntryType = "GameObject"
gameObjectGroupObjectDataEntryType = "Group"
inventoryObjectDataEntryType = "Inventory"
rpgStatObjectDataEntryType = "Stats"

objectDataEntryTypePrimitives = [stringObjectDataEntryType,
								 intObjectDataEntryType,
								 floatObjectDataEntryType]

# bracketMap is a mapping of open brackets in save strings. This is used to indicate what scopes should be ignored when
# splitting a string while loading it from a file
bracketMap = {'(': ')', '[': ']', '{': '}', '\"': '\"'}

##### BEGIN INVENTORY SAVE TOKENS #####

# The inventory is saved using a combination of loadout slots and items placed in the inventory. An inventory is
# surrounded by braces, and then within the braces, individual entries in the inventory are seperated by commas. Each
# entry consists of two components, the information for the item, and a classification of what part of the inventory the
# item is in, a loadout slot or the inventory proper. If the item is in the inventory, the classification will be a
# location tuple indicating where in the inventory the item is. Otherwise it will be a token indicating what kind of
# slot the item is in.

utilitySlotToken = "UtilitySlot"
weaponSlotToken = "WeaponSlot"

inventoryPosStartToken = "("
inventoryPosEndToken = ")"

noneObjectDataName = None
noneObjectDataModule = None
