extends Node


##### DEBUG #####
var debug = False
var triggerDebug = False
var interactionDebug = False
var mapLoadDebug = False
var loadFailureDebug = False
var combatDebug = False
var projectileDebug = True
var weaponDebug = False
var inventoryDebug = False
var imageCollectionDebug = False
var animationDebug = False
var terrainDebug = False
var pathfindingDebug = False
var collisionBoxDebug = False
var rpgDebug = False
var groupLoadDebug = False

var showPerfectAimingInformationDebug = False

var showFPS = True

var targetSearchDebug = False

##### END DEBUG #####

const secondsPerMinute = 60.0

##### Image Constants #####

const inventoryImageString = "inventory"

##### Weapon Constants #####

const semiAutoString = "semi"
const fullAutoString = "auto"
var engageAngle =  5.0

##### Control Constants #####
const leftMouseButton = "leftMouse"
const rightMouseButton = "rightMouse"
const middleMouseButton = "middleMouse"
const mouseScrollUp = "Mouse Scroll Up"
const mouseScrollDown = "Mouse Scroll Down"
const ctrl = "ctrl"
const alt = "alt"
const shift = "shift"

const up = 0
const right = 1
const down = 2
const left = 3

const clockwise = 3
const counterclockwise = 9

const shootingControlScheme = "ShooterScheme"
const commanderControlScheme = "CommanderScheme"
const inventoryControlScheme = "InvScheme"
const uiControlScheme = "ui"
const cinematicControlScheme = "cinematic"
const mercuryControlScheme = "Mercury"

const weaponString = "weapon"
const utilityString = "utility"

const primaryWeaponSlot = 0
const secondaryWeaponSlot = 1

const baseImageString = "base"
const deadImageString = "dead"


##### UI TYPES #####
const DEFAULT_UI = "default"
const EDITOR_UI = "editor"

##### SIDE CONSTANTS #####
const playerSide = "BLUFOR"
const opponentSide = "OPFOR"
const civSide = "CIV"
const neutralSide = "GREENFOR"

##### INTERACTION CONSTANTS #####
const touchingDistance = 100

const defaultSaveFile = os.path.join("game", "maps", "SaveGame")

const collisionBoxToken = "Collision:"
const leftBoxToken = "Left:"
const topBoxToken = "Top:"
const widthBoxToken = "Width:"
const heightBoxToken = "Height:"
