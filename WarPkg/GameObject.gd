extends KinematicBody2D

from WarPkg.Constants.warConsts import *
import WarPkg.Constants.SaveStringTokens as SaveStringTokens

import WarPkg.Graphics.Util.ImageCollection

import WarPkg.BaseGameObjects.GameObjectManagers.RotationManager as RotationManager
import WarPkg.BaseGameObjects.GameObjectManagers.PhysicsManager as PhysicsManager

import WarPkg.Util.ObjectData as ObjectData
import WarPkg.Util.MathFunctions as MathFunctions
import WarPkg.Util.SaveLoadUtilities.ConvertStringToObjectFunctions as ConvertStringToObjectFunctions
import WarPkg.Util.Geometry.Hitbox as Hitbox
import WarPkg.Graphics.Sprites.Sprite as Sprite
import WarPkg.Graphics.Sprites.NullSprite as NullSprite
import WarPkg.Graphics.GraphicsConstants as GraphicsConstants

import editor.editorConsts as editorConsts
import WarPkg.Util.Geometry.MyVector as MyVector

# The following are the displayed strings for various parameters in the editor. They will be seen when level editors
# try to edit objects directly
const displayNameString = "Display Name"
const uidString = "Unique ID"
const xCoordString = "X Coordinate"
const yCoordString = "Y Coordinate"
const connectedObjectUIDString = "Connected Object UIDs:"

# The following are tokens used to save parameter/value pairs to map data files
const DISPLAYNAMEToken = "displayName"
const connectedObjectUIDsToken = "ConnectedObjectUIDs"

##### Editable Field Strings #####
const widthString = "Width"
const heightString = "Height"

##### ObjectData Tokens #####
const widthToken = "width"
const heightToken = "height"


class_name GameObject


class GameObject(object):
	# Fields that can be edited precisely using a text box in the editor. Keys are the parameter to be edited, values
	# are the data types accepted by the parameter. If you are extending game object, note that you should append
	# GameObject's editableFields to that object's editableFields information
	editableFields = {
		displayNameString: SaveStringTokens.STRING_FIELD,
		uidString: SaveStringTokens.STRING_FIELD,
		xCoordString: SaveStringTokens.INT_FIELD,
		yCoordString: SaveStringTokens.INT_FIELD}

	def __init__(self, model, name=None, displayName=None, imageCollectionName=None, loc=(0, 0)):
		self.model = model
		self.name = name  # Internal Name used for loading and saving maps
		self.displayName = displayName  # Name used to identify the object in-game
		# An object containing all the data necessary for loading and saving this object
		self.objectData = WarPkg.Util.ObjectData.objectData(self.model.getModules(),
															self.getName(),
															self.getModuleName())
		self.uid = "-1"  # The object's Unique Identifier. String used to identify unique objects in the model.

		##### TYPE IDENTIFIERS #####
		self.isActor = False
		self.isTerrain = False
		self.isBackground = False
		self.isAesthetic = False
		self.isTrigger = False
		self.isEffect = False
		self.isCondition = False
		self.isInteractable = False
		self.isStretchable = False
		self.isRotatable = False
		self.isWaypoint = False
		self.isItem = False
		self.isMovable = False

		self.location = loc  # The GameObject's location in the level
		self.speed = (0, 0)  # Both the direction and magnitude of the object's current movement as a vector

		self.sprite = None
		self.spriteName = None

		##### Rotation Variables #####
		self.rotation = 0.0
		self.rotationManager = RotationManager.RotationManager(self)

		self.physicsManager = PhysicsManager.PhysicsManager(self)

		# The Terrain tiles currently being occupied by this GameObject
		self.leftQuadrant = 0
		self.topQuadrant = 0
		self.rightQuadrant = 0
		self.bottomQuadrant = 0

		# The UIDs of all objects that are connected to this object. Connecting objects can have several different
		# meanings depending on the type of objects being connected.
		self.connectedObjectUIDs = []
		# Determines whether or not the connected object UIDs should be displayed in the textbox based editor menu to be
		# precisely edited
		self.showConnectedObjectUIDsEditMenu = False

		self.inventory = None
		self.hasInventory = False

		# True if we should check if we should perform collision detection for this object or not
		self.supportsCollisions = True
		# True if astar pathfinding should take this object into account
		self.affectsAStarPathfinding = False
		# True if whether or this object colllides can be changed
		self.collisionsMutable = False

		# A simple list of all of the sounds associated with this GameObject. Simplifies the process of updating all
		# sounds for a gameObject
		self.warSounds = []

		# typeTags is a list of strings that other game objects can use to identify certain non standard attributes of a
		# game object here. For example, if a weapon is one handed, or if a person qualifies has being heavily armoured,
		# that would be indicated by a string in this list that other objects check for
		self.typeTags = list()

		# Variables used to initialize the collisionbox to the appropriate width and height. This is only used for
		# stretchable objects
		self.initWidth = 0
		self.initHeight = 0

		self.graphicsLayerName = GraphicsConstants.miscGameObjectGraphicsLayerName

		# stat modifiers is a list of all stat modifiers applicable to this game object. A GameObject's stat modifiers
		# are used eclectically. For example, a gun might have a damage increate stat modifier which is applied as part
		# of a damage event
		self.statModifiers = list()

	def getMaxSpeed(self):
		# By default, most game objects cannot move so their max speed is 0
		return 0

	def addStatModifier(self, statMod):
		if statMod not in self.getStatModifiers():
			self.getStatModifiers().append(statMod)

	def getStatModifiers(self):
		return self.statModifiers

	# ModifyStat(stat, event) takes in a stat and optionally and RPG stat event and requests that each of the stat mods
	# in our stat modifier list modifies the stat given the context provided by event
	def modifyStat(self, stat, event=None):
		for statMod in self.getStatModifiers():
			statMod.modifyStat(stat, event)

	# If an item is Placeable it means it shows up in the editor as  placeable objects
	# generally items and abstract objects should not be, but anything else should
	@staticmethod
	def isPlaceable():
		return True

	@staticmethod
	def isHoldableItem():
		return False

	@staticmethod
	def isAWaypoint():
		return False

	##### GETTERS #####
	def getObjectData(self):
		return self.objectData

	def getName(self):
		return type(self).__name__

	def getDisplayName(self):
		return self.displayName
		
	def getAffectsAStarPathfinding(self):
		return self.affectsAStarPathfinding

	def getRotationManager(self):
		return self.rotationManager

	def getPhysicsManager(self):
		return self.physicsManager
		
	def getSprite(self):
		return self.sprite

	def getSpriteName(self):
		return self.spriteName

	def getLocation(self):
		return self.location

	def getX(self):
		return self.location[0]

	def getY(self):
		return self.location[1]

	def getRotation(self):
		return self.rotation

	def getHbox(self):
		return self.Hbox

	def canMove(self):
		return self.isMovable

	def setHbox(self, hbox):
		self.Hbox = hbox

	# getOriginalRect() gets the collisionBox of the Terrain without any stretching done
	def getOriginalRect(self):
		return self.getSprite().buildCollisionBox()

	def getCollisionBox(self):
		return self.collisionBox

	def setCollisionBox(self, collisionBox):
		self.collisionBox = collisionBox

	def getUID(self):
		return self.uid

	def getSpeed(self):
		return self.speed

	def getModel(self):
		return self.model

	def getCenter(self):
		return self.getCollisionBox().center

	def getLeft(self):
		return self.getCollisionBox().left

	def getRight(self):
		return self.getCollisionBox().right

	def getTop(self):
		return self.getCollisionBox().top

	def getBottom(self):
		return self.getCollisionBox().bottom

	def getInventory(self):
		return self.inventory

	# TODO Move all width, height, and location information to the collisionBox
	def getRightCoordinate(self):
		return self.location[0] + self.getCollisionBox().width

	def getLeftCoordinate(self):
		return self.location[0]

	def getLeftQuadrant(self):
		return self.leftQuadrant

	def getRightQuadrant(self):
		return self.rightQuadrant

	def getTopQuadrant(self):
		return self.topQuadrant

	def getBottomQuadrant(self):
		return self.bottomQuadrant

	# returns the TopLeft Corner Quadrant
	def getTopLeftQuadrant(self):
		return self.getLeftQuadrant(), self.getTopQuadrant()

	def getBottomRightQuadrant(self):
		return self.getRightQuadrant(), self.getBottomQuadrant()

	def getBottomCoordinate(self):
		return self.location[1] + self.getCollisionBox().height

	def getTopCoordinate(self):
		return self.location[1]

	def getWarSounds(self):
		return self.warSounds

	def getTypeTags(self):
		return self.typeTags

	def getGraphicsLayerName(self):
		return self.graphicsLayerName

	def addTypeTag(self, typeTag):
		self.getTypeTags().append(typeTag)

	# hasTypeTag(tag) returns true if tag is in this object's list of type tags
	def hasTypeTag(self, tag):
		if tag in self.getTypeTags():
			return True
		return False

	def isMoving(self):
		if self.getSpeed()[0] != 0:
			return True
		if self.getSpeed()[1] != 0:
			return True
		return False

	def addItem(self, item):
		self.inventory.insertAnywhere(item)

	def collides(self):
		return self.supportsCollisions

	# collidesWith(otherObject) returns true if this object should collide with this object. If other object is this
	# object, or one or both of the objects don't collide, this should return false
	def collidesWith(self, otherObject):
		if self is otherObject:
			return False
		if not otherObject.collides():
			return False
		if not self.collides():
			return False

		return True

	# canAccessInventory() only returns true if the player can access this object's inventory. This only checks if the
	# player is allowed to. It does not check for things like the distance between the player and the object.
	def canAccessInventory(self):
		return self.hasInventory

	# Returns true if the object can be rotated in the editor and false otherwise
	def canRotate(self):
		return False

	def toString(self):
		return "Name: " + self.getName() + " X: " + str(self.getX()) + " Y: " + str(self.getY())
	##### END GETTERS #####

	# addVectorToLocation(vector) Takes a vector as input and adds the x and y values of the vector to that location
	# this is to help facilitate using velocity vectors to change the location
	def addVectorToLocation(self, vector):
		newLocX = self.getX() + vector.getX()
		newLocY = self.getY() + vector.getY()

		self.setLoc((newLocX, newLocY))

	##### SETTERS #####

	def setLoc(self, newLoc):
		self.location = newLoc
		if self.getCollisionBox() is not None:
			self.getCollisionBox().x = newLoc[0]
			self.getCollisionBox().y = newLoc[1]

	def setLocation(self, newLoc):
		self.setLoc(newLoc)

	def setCenter(self, newLoc):
		self.getCollisionBox().center = newLoc
		self.location = (self.getCollisionBox().x, self.getCollisionBox().y)

	def setCenterX(self, newX):
		newCenter = (newX, self.getCenter()[1])
		self.setCenter(newCenter)

	def setCenterY(self, newY):
		newCenter = (self.getCenter()[0], newY)
		self.setCenter(newCenter)

	def setLeft(self, newLoc):
		self.getCollisionBox().left = newLoc
		self.location = (self.getCollisionBox().x, self.getCollisionBox().y)

	def setRight(self, newLoc):
		self.getCollisionBox().right = newLoc
		self.location = (self.getCollisionBox().x, self.getCollisionBox().y)

	def setTop(self, newLoc):
		self.getCollisionBox().top = newLoc
		self.location = (self.getCollisionBox().x, self.getCollisionBox().y)

	def setBottom(self, newLoc):
		self.getCollisionBox().bottom = newLoc
		self.location = (self.getCollisionBox().x, self.getCollisionBox().y)

	def setName(self, n):
		self.name = n

	def setUID(self, newUID):
		self.uid = newUID

	def setObjectData(self, objectData):
		self.objectData = objectData

	def setRotation(self, newRotation):
		# Keep the Azimuth within 0-360 degrees
		if newRotation > 360.0:
			newRotation = newRotation - 360.0
		elif newRotation < 0.0:
			newRotation = newRotation + 360.0
		self.rotation = newRotation
		# self.rotation = MathFunctions.myRound(newRotation, ROTATION_LOCK)

	def setX(self, loc):
		self.location = (int(loc), int(self.getY()))
		self.updatePosition()

	def setY(self, loc):
		self.location = (int(self.getX()), int(loc))
		self.updatePosition()

	def setModel(self, model):
		self.model = model

	def setSprite(self, mySprite):
		self.sprite = mySprite

	def setSpeed(self, xSpeed, ySpeed):
		self.speed = (xSpeed, ySpeed)

	def addWarSound(self, myWarSound):
		self.warSounds.append(myWarSound)

	##### END SETTERS #####

	# Returns true if this object is to the left of pt
	def isLeft(self, pt):
		if pt[0] < self.getX():
			return True
		return False

	# Returns true if this object is to the right of pt
	def isRight(self, pt):
		if pt[0] > self.getX():
			return True
		return False

	# Returns true if this object is above pt
	def isUp(self, pt):
		if pt[1] < self.getY():
			return True
		return False

	# Returns true if this object is below pt
	def isDown(self, pt):
		if pt[1] > self.getY():
			return True
		return False

	def makeSprite(self):
		if self.getSpriteName() is not None:
			self.setSprite(Sprite.Sprite(self, self.getSpriteName()))
		else:
			self.setSprite(NullSprite.NullSprite(self))

	# initAssets() initalizes all of the assets (graphics, sounds, etc.) That need to be loaded into memory for this
	# GameObject. It will request the assets from the model's asset managers, which will then either load the asset from
	# file or simply return the asset if it has already been loaded by another object
	def initAssets(self):
		self.makeSprite()
		self.getSprite().initAssets()

	# init() is a function that prepares the GameObject to be used in a level
	def init(self):
		self.initAssets()

		# Update the hit and collision boxes
		self.setCollisionBox(Hitbox.HitBox(self.getSprite().buildCollisionBox()))
		self.getCollisionBox().x = self.getX()
		self.getCollisionBox().y = self.getY()
		self.getCollisionBox().update()
		self.setHbox(Hitbox.HitBox(self.getSprite().buildHitBox()))
		self.getHbox().x = self.getX()
		self.getHbox().y = self.getY()

		self.getObjectData().setModules(self.model.getModules())

		if self.isStretchable:
			# The Terrain may be stretched by it being initialized so the collisionBox may need to be updated
			self.updateCollisionBox(width=self.initWidth, height=self.initHeight)

		self.updateQuadrants()

		self.setObjectRefsFromUIDs()

	# updatePosition() updates the collisionBox and hitBox to reflect the object's current location
	def updatePosition(self):
		self.getCollisionBox().x = self.getX()
		self.getCollisionBox().y = self.getY()
		self.getHbox().x = self.getX()
		self.getHbox().y = self.getY()
		
	def updateAStarPathfindingGrid(self):
		if self.getAffectsAStarPathfinding():
			theNodes = self.getModel().getPathfindingGrid().getIntersectingNodes(self.getCollisionBox())
			for myNode in theNodes:
				self.getModel().getPathfindingGrid().makeNodeFromModel(myNode)
			
	# updateQuadrants() updates the tiles of this object's model's terrainMap to indicate which terrain tiles it is
	# occupying
	def updateQuadrants(self):
		changed = False
		if self.model is not None:
			leftX = self.getCollisionBox().left
			topY = self.getCollisionBox().top
			rightX = self.getCollisionBox().right
			bottomY = self.getCollisionBox().bottom
			upperLeft = self.model.terrain.getQuadrant((leftX, topY))
			lowerRight = self.model.terrain.getQuadrant((rightX, bottomY))
			if not(self.leftQuadrant == upperLeft[0]):
				changed = True
			if not(self.topQuadrant == upperLeft[1]):
				changed = True
			if not(self.rightQuadrant == lowerRight[0]):
				changed = True
			if not(self.bottomQuadrant == lowerRight[1]):
				changed = True
			if changed:
				for x in range(self.leftQuadrant, self.rightQuadrant+1):
					for y in range(self.topQuadrant, self.bottomQuadrant+1):
						myTile = self.model.terrain.getTerrainTile(x, y)
						if myTile is not None:
							if self in myTile:
								myTile.remove(self)
				self.leftQuadrant = upperLeft[0]
				self.topQuadrant = upperLeft[1]
				self.rightQuadrant = lowerRight[0]
				self.bottomQuadrant = lowerRight[1]
				for x in range(self.leftQuadrant, self.rightQuadrant+1):
					for y in range(self.topQuadrant, self.bottomQuadrant+1):
						myTile = self.model.terrain.getTerrainTile(x, y)
						if myTile is not None:
							myTile.append(self)

	# update(elapsedTime) is the function that is constantly run by this object every iteration of the game loop.
	# elapsedTime is the amount of time that has passed since the last frame in milliseconds
	def update(self, elapsedTime):
		if self.getCollisionBox() is not None:
			self.updatePosition()
			self.updateQuadrants()

		if self.getSprite() is not None:
			self.getSprite().update(elapsedTime)

		for myWarSound in self.getWarSounds():
			myWarSound.update(elapsedTime)

		# self.getPhysicsManager().update(elapsedTime)

		# self.updateObjectData()

	# removeFromModel() removes this object from the model it is part of
	def removeFromModel(self):
		if self in self.model.objectList:
			self.model.objectList.remove(self)
		for x in range(self.leftQuadrant, self.rightQuadrant+1):
			for y in range(self.topQuadrant, self.bottomQuadrant+1):
				if self in self.model.terrain.tiles[y][x]:
					self.model.terrain.tiles[y][x].remove(self)
		self.updateAStarPathfindingGrid()

	###############################################################################################################
	# MethodName: initFromObjectData
	#
	# Remarks: Every GameObject in WAR had an associated ObjectData object. This object is used to store various
	# types of information about the state of the object, such as its location, and its name. ObjectData objects
	# Consist of a series of key-value pairs which are read in from files by a function in util.py. However, the
	# data cannot be then moved into the object automatically. These functions map variables to keys.n This data
	# is used to create maps and save and load files for the game.
	#
	# Contract: initFromObjectData()->None
	#
	# Effects: Changes several variables based on the contests of this GameObject's ObjectData
	#
	# Author: Jason Wolter (Last Modified September 19th 2018)
	###############################################################################################################
	def initFromObjectData(self):
		self.name = self.objectData.getData(SaveStringTokens.NAMEToken)
		self.uid = self.objectData.getData(SaveStringTokens.UIDToken)
		if self.objectData.getData(SaveStringTokens.LOCATIONXToken) is not None:
			self.location = (int(float(self.objectData.getData(SaveStringTokens.LOCATIONXToken))),
							 int(float(self.objectData.getData(SaveStringTokens.LOCATIONYToken))))
		self.displayName = self.objectData.getData(DISPLAYNAMEToken)

		if self.isRotatable:
			if self.objectData.getData(SaveStringTokens.RotationToken) is not None:
				self.rotation = self.getObjectData().getData(SaveStringTokens.RotationToken)

		self.connectedObjectUIDs = self.objectData.getData(connectedObjectUIDsToken)
		# None indicates an empty UID list, so we should set it to an empty List
		if self.connectedObjectUIDs is None:
			self.connectedObjectUIDs = []

		if self.isStretchable:
			if self.objectData.getData(widthToken) is not None:
				self.initWidth = int(self.objectData.getData(widthToken))  # The orientation of the Terrain
			if self.objectData.getData(heightToken) is not None:
				self.initHeight = int(self.objectData.getData(heightToken))  # The length of the Terrain

	###############################################################################################################
	# MethodName: initObjectData
	#
	# Remarks: Initializes the ObjectData variable for this GameObject. See initFromObjectData for an
	# Explanation of how the ObjectData Variable is used.
	#
	# Contract: initObjectData()->None
	#
	# Effects: Sets the values in the objectData object based on the actual values of the variables of the gameObject.
	#
	# Author: Jason Wolter (Last Modified September 19th 2018)
	###############################################################################################################
	def initObjectData(self):
		self.objectData.addStringDataEntry(SaveStringTokens.NAMEToken, self.getName())
		self.objectData.addStringDataEntry(SaveStringTokens.MODULEToken, self.getModuleName())
		self.objectData.addStringDataEntry(SaveStringTokens.UIDToken, self.uid)
		self.objectData.addFloatDataEntry(SaveStringTokens.LOCATIONXToken, self.location[0])
		self.objectData.addFloatDataEntry(SaveStringTokens.LOCATIONYToken, self.location[1])
		self.objectData.addStringDataEntry(DISPLAYNAMEToken, self.displayName)

	def updateObjectData(self):
		###############################################################################################################
		# Author: Jason Wolter (Last Modified 03/07/2017)
		# Remarks: updateObjectData() automatically updates the GameObject's objectData field to contain all of the
		# object's relevant fields so that the object can be recreated in it's current state by creating a new object
		# using this object's ObjectData field. This should only be used by the EDITOR
		# Input:	None
		#
		# Output:   None
		###############################################################################################################
		self.updateUIDs()

		self.objectData.addStringDataEntry(SaveStringTokens.NAMEToken, self.getName())
		self.objectData.addStringDataEntry(SaveStringTokens.MODULEToken, self.getModuleName())
		self.objectData.addStringDataEntry(SaveStringTokens.UIDToken, self.uid)
		self.objectData.addFloatDataEntry(SaveStringTokens.LOCATIONXToken, self.location[0])
		self.objectData.addFloatDataEntry(SaveStringTokens.LOCATIONYToken, self.location[1])
		self.objectData.addStringDataEntry(DISPLAYNAMEToken, self.displayName)

		self.objectData.addListDataEntry(connectedObjectUIDsToken, self.connectedObjectUIDs)

		if self.isRotatable:
			self.objectData.addFloatDataEntry(SaveStringTokens.RotationToken, self.getRotation())

		if self.isStretchable:
			self.objectData.addFloatDataEntry(widthToken, self.getCollisionBox().getWidth())
			self.objectData.addFloatDataEntry(heightToken, self.getCollisionBox().getHeight())

	# updateUIDs is a function that takes all of the UIDs that this object uses to reference other objects and updates
	# them so that the UIDs match up with the actual objects this object is currently referencing. This will have
	# different results depending on the GameObject, with other GameObjects
	def updateUIDs(self):
		thisFunctionDoesNothing = True

	# setObjectRefsFromUIDS() Uses the GameObject's UID variables to assign the variables to reference other objects
	# directly. For example, if this GameObject was an effect meant to raise a character's health, this would use the
	# character's UID to set a variable referencing the character directly
	def setObjectRefsFromUIDs(self):
		thisFunctionDoesNothing = True

	# getModuleName() Gets the name of the module this GameObject is contained by
	def getModuleName(self):
		return self.__module__

	##### Editable Field Functions #####

	###############################################################################################################
	# MethodName: getEditableFieldData
	#
	# Remarks: In the WarEditor, the values of some GameObject Variables can be directly edited by text entry
	# The Editor uses the EditableFieldData dictionary to both determine what variables these are, what their
	# current values are and to label them in a human readable format.
	#
	# Contract: getEditableFieldData()->EditableFields Dictionary
	#
	# Effects: None
	#
	# Author: Jason Wolter (Last Modified September 19th 2018)
	###############################################################################################################
	def getEditableFieldData(self):
		editableFieldData = dict()
		editableFieldData[displayNameString] = self.displayName
		editableFieldData[uidString] = self.uid
		editableFieldData[xCoordString] = self.location[0]
		editableFieldData[yCoordString] = self.location[1]

		# Don't want to show connected objects for every type of object:
		if self.showConnectedObjectUIDsEditMenu:
			editableFieldData[connectedObjectUIDString] = \
				ConvertStringToObjectFunctions.getStringFromList(self.connectedObjectUIDs)

		return editableFieldData

	# Sets the values of GameObject variables based on data entered into the editableFieldData Dictionary.
	def setEditableFieldData(self, editableFieldData):
		self.displayName = editableFieldData[displayNameString]
		self.uid = editableFieldData[uidString]
		x = int(editableFieldData[xCoordString])
		y = int(editableFieldData[yCoordString])
		self.location = (x, y)

		if self.showConnectedObjectUIDsEditMenu:
			self.connectedObjectUIDs = \
				ConvertStringToObjectFunctions.getListFromString(editableFieldData[connectedObjectUIDString])

	###############################################################################################################
	# MethodName: connect
	#
	# Remarks: Connect is a tool in the WarEditor. It is used to contextually relate two GameObjects. For Example,
	# A computer and a door when connected might allow the computer to open the door. This is an abstract function
	# used to facilitate that tool. When one object is connected to another, the otherObject is also automatically
	# connected to it as well using the same function. The "connected" boolean parameter is used to prevent infinite
	# Recursion
	#
	# Contract: connect(GameObject)->None
	#
	# Effects: Connects two GameObjects based on context.
	#
	# Author: Jason Wolter (Last Modified September 19th 2018)
	###############################################################################################################
	def connect(self, otherObject, connected=False):
		if not connected:
			if otherObject is not None:
				otherObject.connect(self, connected=True)

	###############################################################################################################
	# MethodName: shouldPaint
	#
	# Remarks: Used to determine if an object should currently be painted
	#  		   Used for thing like not having line of sight to an actor
	#
	# Contract: shouldPaint()->Boolean
	#
	# Effects: Determines if an actor should be painted
	#
	# Author: Jason Wolter (Last Modified December 23rd 2018)
	###############################################################################################################
	def shouldPaint(self):
		return True

	# paint(surf, camera) renders the appropriate image to represent self to surf based on data from camera
	def paint(self, surf, camera):
		if self.shouldPaint():
			self.getSprite().paint(surf, camera)

			if collisionBoxDebug:
				pygame.draw.rect(surf, (0, 0, 255), camera.applyRect(self.getCollisionBox()), 5)

	def drawHighlighted(self, surf, camera):
		if self.shouldPaint():
			self.getSprite().drawHighlightRect(surf, camera)

	def drawSelected(self, surf, camera):
		if self.shouldPaint():
			self.getSprite().drawSelectionRect(surf, camera)

	# paintConnections(surf, camera) paints lines from this object to objects it is connected to on surf based on data
	# from camera.
	def paintConnections(self, surf, camera):
		myCenter = camera.applyGeneric(self.getCollisionBox().center)
		# Looping through every single object in the model. This may eventually become infeasible.
		for uid in self.model.objectList:
			otherObject = self.model.getFromObjectList(uid)
			if otherObject is not None:
				# Checking if the otherobject is connected, if so, we draw a line to it
				if self.isConnected(otherObject):
					otherCenter = camera.applyGeneric(otherObject.getCollisionBox().center)
					pygame.draw.line(surf, (0, 0, 255), myCenter, otherCenter)

	# isConnected(otherObject) returns true if this object qualifies as connected to this object and false otherwise.
	# What constitutes a connected object can vary for different game object, however, it is universally true that if
	# the otherObject is connected to this object, this object is by default connected to the other object
	def isConnected(self, otherObject, checkingFromOtherObject=False):
		if not checkingFromOtherObject:
			if otherObject.isConnected(self, checkingFromOtherObject=True):
				return True
		else:
			return False

	# Returns a deep copy of the important components of a gameObject
	def copy(self):
		self.updateObjectData()
		objectCopy = self.getObjectData().makeObjectFromObjectData(self.model)
		objectCopy.model = self.model
		return objectCopy

	def getWidth(self):
		return self.collisionBox.getWidth()

	def getHeight(self):
		return self.collisionBox.getHeight()

	def makeSaveString(self, depth=0):
		return self.getObjectData().makeSaveString(depth)

	# getDataType() returns the dataType string for saving this object to a SaveString statement
	def getDataType(self):
		return SaveStringTokens.gameObjectObjectDataEntryType

	def checkMinimumDistance(self, otherObject):
		otherCollisionBox = otherObject.getCollisionBox()
		horizontalDistance = 0
		verticalDistance = 0
		if self.getCollisionBox().right < otherCollisionBox.left:
			horizontalDistance = self.getCollisionBox().right - otherCollisionBox.left

	def getOccupiedQuadrants(self):
		return self.getModel().terrain.getQuadrantListFromCorners(self.getTopLeftQuadrant(),
																  self.getBottomRightQuadrant())

	def logDebug(self, debugString):
		self.getModel().logDebug(debugString)

	def checkCollisionsAtPoint(self, pt):
		newCollisionBox = Hitbox.HitBox(self.getCollisionBox())
		newCollisionBox.center = pt

		topLeftTile = self.getModel().terrain.getQuadrant(newCollisionBox.topleft)
		bottomRightTile = self.getModel().terrain.getQuadrant(newCollisionBox.bottomright)

		minXTile = topLeftTile[0]
		maxXTile = bottomRightTile[0]
		minYTile = topLeftTile[1]
		maxYTile = bottomRightTile[1]

		x = minXTile
		while x <= maxXTile:
			y = minYTile
			while y <= maxYTile:
				i = 0
				while i < len(self.getModel().terrain.tiles[y][x]):
					if self.getModel().terrain.tiles[y][x][i] != self:
						if self.getModel().terrain.tiles[y][x][i].collides():
							if newCollisionBox.colliderect(self.getModel().terrain.tiles[y][x][i].getCollisionBox()):
								return True
					i += 1
				y += 1
			x += 1
			return False

	def isSelected(self):
		if self.getModel().objectIsSelected(self):
			return True
		return False

	# shouldSelectPoint(pt) takes a point and returns true if clicking that point should select this object and false
	# otherwise
	def shouldSelectPoint(self, pt):
		if self.getCollisionBox():
			if self.getCollisionBox().collidepoint(pt):
				return True
		return False

	# shouldSelectRect(rect) takes a selection rectangle and returns true if that selection rectangle should select this
	# object
	def shouldSelectRect(self, rect):
		if self.getCollisionBox():
			if self.getCollisionBox().colliderect(rect):
				return True
		return False

	def getStretchDirectionsFromPoint(self, pt):
		return self.getCollisionBox().getStretchDirectionsFromPoint(pt)

	def shouldStretchFromPoint(self, pt):
		if self.isStretchable:
			if self.getStretchDirectionsFromPoint(pt):
				return True
		return False

	def shouldRotateFromPoint(self, point):
		if self.isRotatable:
			return self.getRotationManager().shouldRotateFromPoint(point)
		return False

	###############################################################################################################
	# MethodName: updateCollisionBox
	#
	# Remarks: This function updates the collisionBox so that it is properly rotated, and
	# that it has the proper length. The Terrain element can be stretched and rotated horizontally
	# or vertically in the editor, so this function is to deal with that.
	#
	# Contract: updateCollisionBox((Opt) boolean)->None
	#
	# Effects: Changes the CollisionBox of the Terrain
	#
	# Author: Jason Wolter (Last Modified September 19th 2018)
	###############################################################################################################
	def updateCollisionBox(self, width=None, height=None, stretchDirections=None, box=None, origRect=None, useMinimumDimensions=True):
		if not box and self.getCollisionBox():
			box = self.getCollisionBox()
		if not origRect and self.getOriginalRect():
			origRect = self.getOriginalRect()
		if stretchDirections is None:
			stretchDirections = list()
		if box is not None:
			if width is not None:
				box.width = width
				if useMinimumDimensions and origRect is not None:
					box.makeMinimumWidth(origRect.width, stretchDirections)
			if height is not None:
				box.height = height
				if useMinimumDimensions and origRect is not None:
					box.makeMinimumHeight(origRect.height, stretchDirections)

			box.update()

	# updateDimensions() keeps all of the other components of a gameObject up to date with its collision box including
	# its sprite and its quadrants
	def updateDimensions(self):
		self.getSprite().updateSlicedImage()
		if self.getModel().getFromObjectList(self.getUID()) is self:
			self.updateQuadrants()

	# setDimension() performs a number of actions required to set the dimension of this object including changing the
	# size of its collision box, updating its sprite, and updating the quadrants that contain it
	def setDimension(self, width, height):
		self.updateCollisionBox(width=width, height=height)
		self.updateDimensions()

	# Resets the object's dimensions to their originals
	def resetDimension(self):
		self.setDimension(self.getOriginalRect().getWidth(), self.getOriginalRect().getHeight())

	# getLineOfSightCollisions(otherObject, excludeList, includeFunction) Returns all of the collisions
	def getLineOfSightCollisions(self, otherObject, excludeList=None, includeFunction=None):
		# If we're not passed an exclude list, make one
		if not excludeList:
			excludeList = list()

		# Add ourselves and the other object to the list of objects not included in the collision list
		excludeList.append(self)
		excludeList.append(otherObject)

		# A vector from our center to the center of the object we're trying to get line of sight to
		collisionCheckVector = MyVector.MyVector(origin=self.getCenter(), endPoint=otherObject.getCenter())

		collisions = collisionCheckVector.getAllCollisions(self.getModel().terrain, excludeList=excludeList)

		return collisions

	def hasLineOfSight(self, otherObject, excludeList=None):
		if not self.getLineOfSightCollisions(otherObject, excludeList):
			return True
		return False

	def stretchInDirections(self, pos, directions):
		self.getCollisionBox().stretchInDirections(pos, directions)
		self.updateCollisionBox(width=self.getCollisionBox().width,
								height=self.getCollisionBox().height,
								stretchDirections=directions)
		self.setLoc((self.getCollisionBox().x, self.getCollisionBox().y))

		self.updateDimensions()
